<script src="{{asset('frontend/js/jquery.jquery.min.js')}}"></script>

<script src="{{asset('frontend/js/bootstrap-select.js')}}"></script>

<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>

<script src="{{asset('frontend/jseasyResponsiveTabs.js')}}"></script>

<script src="{{asset('frontend/js/filedrag.js')}}"></script>

<script src="{{asset('frontend/js/jquery.fileupload.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.flexisel.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.flexslider.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.iframe-transport.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.knob.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.leanModal.min.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.ui.widget.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.core.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.data.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.data.utils.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.languagefilter.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.lcd.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery.uls.regionfilter.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery-1.10.2.js')}}"></script>

<script src="{{asset('frontend/js/jquery.jquery-ui.js')}}"></script>

<script src="{{asset('frontend/js/jquery.tabs.js')}}"></script>

<script src="{{asset('frontend/js/script.js')}}"></script>

<script>
	$( document ).ready( function() {
		$( '.uls-trigger' ).uls( {
			onSelect : function( language ) {
				var languageName = $.uls.data.getAutonym( language );
				$( '.uls-trigger' ).text( languageName );
			},
					quickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
				} );
	} );
</script>
<script src="js/tabs.js"></script>

<script type="text/javascript">
	$(document).ready(function () {    
		var elem=$('#container ul');      
		$('#viewcontrols a').on('click',function(e) {
			if ($(this).hasClass('gridview')) {
				elem.fadeOut(1000, function () {
					$('#container ul').removeClass('list').addClass('grid');
					$('#viewcontrols').removeClass('view-controls-list').addClass('view-controls-grid');
					$('#viewcontrols .gridview').addClass('active');
					$('#viewcontrols .listview').removeClass('active');
					elem.fadeIn(1000);
				});                     
			}
			else if($(this).hasClass('listview')) {
				elem.fadeOut(1000, function () {
					$('#container ul').removeClass('grid').addClass('list');
					$('#viewcontrols').removeClass('view-controls-grid').addClass('view-controls-list');
					$('#viewcontrols .gridview').removeClass('active');
					$('#viewcontrols .listview').addClass('active');
					elem.fadeIn(1000);
				});                                 
			}
		});
	});
</script>

<script type="text/javascript">
	$(window).load(function() {
		$("#flexiselDemo3").flexisel({
			visibleItems:1,
			animationSpeed: 1000,
			autoPlay: true,
			autoPlaySpeed: 5000,            
			pauseOnHover: true,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: { 
				portrait: { 
					changePoint:480,
					visibleItems:1
				}, 
				landscape: { 
					changePoint:640,
					visibleItems:1
				},
				tablet: { 
					changePoint:768,
					visibleItems:1
				}
			}
		});

	});
</script>